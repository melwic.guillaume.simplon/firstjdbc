DROP DATABASE IF EXISTS first_jdbc;
CREATE DATABASE first_jdbc;
USE first_jdbc;
DROP TABLE IF EXISTS dog;
CREATE TABLE dog(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  breed VARCHAR(255),
  `name` VARCHAR(200) NOT NULL,
  birthdate DATE
);
INSERT INTO
  dog (breed, `name`, birthdate)
VALUES
  ("labrador", "rufus", "2021-06-15"),("shiba inu", "cookie", "2020-04-06"),("husky", "snow", "2019-11-26");