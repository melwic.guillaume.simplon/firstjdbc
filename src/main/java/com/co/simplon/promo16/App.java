package com.co.simplon.promo16;

import java.time.LocalDate;

import com.co.simplon.promo16.entity.Dog;
import com.co.simplon.promo16.repository.DogRepository;
import com.co.simplon.promo16.repository.IDogRepository;

public class App {
    public static void main( String[] args ){
        IDogRepository repo = new DogRepository();
        System.out.println(repo.findById(2));
        // Dog dog = new Dog("fromjava", "coffee", LocalDate.of(2021, 12, 1));
        // repo.save(dog);
        // System.out.println(dog);
        Dog dog = new Dog(4,"coffee", "java", LocalDate.of(2021, 12, 1));
        repo.update(dog);
        System.out.println(dog);
}
}