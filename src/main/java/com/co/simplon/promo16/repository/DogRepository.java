package com.co.simplon.promo16.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.co.simplon.promo16.entity.Dog;

public class DogRepository implements IDogRepository {

    private Connection connection;

    public DogRepository() {
        try {
            this.connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/first_jdbc");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/first_jdbc");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Dog> findAll() {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            ResultSet result = stmt.executeQuery();
            List<Dog> dogList = new ArrayList<>();
            while (result.next()) {
                Dog dog = new Dog(
                        result.getInt("id"),
                        result.getString("breed"),
                        result.getString("name"),
                        result.getDate("birthdate").toLocalDate());
                dogList.add(dog);
            }
            return dogList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Dog findById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Dog(
                        result.getInt("id"),
                        result.getString("breed"),
                        result.getString("name"),
                        result.getDate("birthdate").toLocalDate());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean save(Dog dog) {
        try {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog(breed, name, birthdate) VALUES (?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getBreed());
            stmt.setString(2, dog.getName());
            stmt.setDate(3, Date.valueOf(dog.getBirthdate()));
            if(stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                dog.setId(result.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Dog dog) {
        try {
            PreparedStatement stmt = connection
            .prepareStatement("UPDATE dog SET breed=?, name=?, birthdate=? WHERE id=?");
            stmt.setString(1, dog.getBreed());
            stmt.setString(2, dog.getName());
            stmt.setDate(3, Date.valueOf(dog.getBirthdate()));

            stmt.setInt(4, dog.getId());

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
