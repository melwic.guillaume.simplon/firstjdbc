package com.co.simplon.promo16.repository;

import java.util.List;

import com.co.simplon.promo16.entity.Dog;

public interface IDogRepository {
    List <Dog> findAll();
    Dog findById(Integer id);
    boolean save(Dog dog);
    boolean update(Dog dog);
    boolean deleteById(Integer id);

}
