package com.co.simplon.promo16.entity;

import java.time.LocalDate;

public class Dog {
    private Integer id;
    private String breed;
    private String name;
    private LocalDate birthdate;

    public Dog(int id, String breed, String name, LocalDate birthdate) {
        this.id = id;
        this.breed = breed;
        this.name = name;
        this.birthdate = birthdate;
    }

    public Dog(String breed, String name, LocalDate birthdate) {
        this.breed = breed;
        this.name = name;
        this.birthdate = birthdate;
    }

    public Dog() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "Dog [birthdate=" + birthdate + ", breed=" + breed + ", id=" + id + ", name=" + name + "]";
    }
}
